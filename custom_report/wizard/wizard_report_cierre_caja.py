from datetime import date, timedelta
from odoo import _, fields, models, exceptions, api
from xlsxwriter.workbook import Workbook
import base64
from io import BytesIO
import pandas as pd
import logging
logger = logging.getLogger(__name__)

states_of_pays = {
    'not_paid':'No Pagado',
    'in_payment':'En Proceso de Pago',
    'paid':'Pagado',
}

class ReportCierreCaja(models.TransientModel):
    _name = "wizard.report.cierre.caja"
    _description = "Wizard for report cierre de caja"

    date_to = fields.Date("Hasta", required=True)
    date_from = fields.Date("Desde", required=True)

    def update_dates(self):
        if self.date_to and self.date_from:
            invoices = self.env['account.move'].search([('invoice_date', '>=', self.date_from), ('invoice_date', '<=', self.date_to)])
            return self.env.ref("custom_report.action_reporte_cierre_caja").report_action(invoices)

    def btn_generate_xlsx(self):
        report_obj = self.env.ref("custom_report.cierrecaja_report_xlsx")
        return report_obj.report_action(self)
        return report_obj.report_action(self, data={"date_from": self.date_from, "date_to": self.date_to})


class ComprobantesXlsx(models.AbstractModel):
    _name = 'report.report_cierrecaja.cierrecaja_report_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def create_xlsx_report(self, docids, data):
        objs = self._get_objs_for_report(docids, data)
        file_data = BytesIO()
        workbook = pd.ExcelWriter(file_data, engine='xlsxwriter')
        self.generate_xlsx_report(workbook, data, obj=objs)
        workbook.save()
        file_data.seek(0)
        return file_data.read(), 'xlsx'

    def generate_xlsx_report(self, workbook, data, obj):
        date_from = data.get("date_from", False)
        date_to = data.get("date_to", False)
        invoices = self.env['account.move'].search([('invoice_date', '>=', obj.date_from), ('invoice_date', '<=', obj.date_to)])
        comps = []
        for invoice in invoices:
            logger.info('Hola Luis')
            logger.info('Hola Luis')
            logger.info('Hola Luis')
            logger.info('Hola Luis')
            pagos = eval(str(invoice.invoice_payments_widget).replace('false','False').replace('true','True').replace('null','"nulo"'))
            logger.info(pagos)
            pago_id = False
            if pagos:
                for pay in pagos['content']:
                    try:
                        if pay['account_payment_id']:
                            pago_id = pay['account_payment_id']
                    except:
                        pass
            if pago_id:
                dirario_pago = self.env['account.payment'].browse(pago_id).journal_id.name
            else:
                dirario_pago = ''


            if invoice.type in ['out_invoice','in_invoice'] and invoice.state == 'posted':
                type = ''
                ref = ''
                num_comp = ''
                total = 0

                if invoice.type == 'out_invoice':
                    type = 'Venta'
                    total = invoice.amount_total
                else:
                    type = 'Compra'
                    ref = invoice.ref
                    num_comp = invoice.inv_supplier_ref
                    total = -invoice.amount_total

                comps.append(
                    {
                        "Socio/Proveedor":invoice.partner_id.name,
                        "Fecha de Factura/Recibo":invoice.invoice_date,
                        "Numero":invoice.name,
                        "Numero Comprobante Proveedor":invoice.name,
                        "Referencia":ref,
                        "Tipo Operacion":type,
                        "Diario":invoice.journal_id.name,
                        "Compañia":invoice.company_id.name,
                        "Total":total,
                        "Estado":states_of_pays[invoice.invoice_payment_state],
                        "Diario Pago":dirario_pago,
                    }
                )

        df = pd.DataFrame(comps, columns=["Socio/Proveedor",
                                          "Fecha de Factura/Recibo",
                                          "Numero",
                                          "Numero Comprobante Proveedor",
                                          "Referencia",
                                          "Tipo Operacion",
                                          "Diario",
                                          "Compañia",
                                          "Total",
                                          "Estado",
                                          "Diario Pago",])
        df.to_excel(workbook, sheet_name='CierreCaja')
        df.style.hide_index()
        logger.info('Hola Luis')
