{
    'name': 'Custom Report ',
    'author': 'Misael',
    'depends': ['base', 'report_xlsx'],
    'data': [
        'reports/cierre_caja.xml',
        'reports/data_reports.xml',
        'wizard/wizard_report_cierre_caja.xml'
    ],
}
